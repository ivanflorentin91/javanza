package com.uaanotas.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.uaanotas.beans.Alumno;

public class AlumnoDao {
	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}

	public int save(Alumno p) {
		String sql = "insert into alumno(nombre,apellido,cedula,direccion) values('" + p.getNombre() + "','" + p.getApellido()
				+ "'," + p.getCedula()+ ",'" + p.getDireccion()+ "')";
		return template.update(sql);
	}

	public int update(Alumno p) {
		String sql = "update alumno set "
                                + "nombre='" + p.getNombre() 
                                + "', apellido='" + p.getApellido() 
                                + "', Cedula='"+ p.getCedula()
                                + "', Direccion='"+ p.getDireccion() 
                                + "'where id=" + p.getId() + "";
		return template.update(sql);
	}

	public int delete(int id) {
		String sql = "delete from alumno where id=" + id + "";
		return template.update(sql);
	}

	public Alumno getAlumnoById(int id) {
		String sql = "select * from alumno where id=?";
		return template.queryForObject(sql, new Object[] { id }, new BeanPropertyRowMapper<Alumno>(Alumno.class));
	}

	public List<Alumno> getAlumnos() {
		return template.query("select * from alumno", new RowMapper<Alumno>() {
			public Alumno mapRow(ResultSet rs, int row) throws SQLException {
				Alumno e = new Alumno();
				e.setId(rs.getInt(1));
				e.setNombre(rs.getString(2));
				e.setApellido(rs.getString(3));
				e.setCedula(rs.getInt(4));
                                e.setDireccion(rs.getString(5));
				return e;
			}
		});
	}
}
