package com.uaanotas.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.uaanotas.beans.Curso;

public class CursoDao {
	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}

	public int save(Curso p) {
		String sql = "insert into curso(descripcion,semestreanho, cantidadinscriptos) "
                        + "values('" + p.getDescripcion()+ "','" + p.getSemestreanho()+"'," + p.getCantidadinscriptos()+ ")";
		return template.update(sql);
	}

	public int update(Curso p) {
		String sql = "update curso set "
                        + "descripcion='" + p.getDescripcion()
                        + "', semestreanho='" + p.getSemestreanho()
                        + "', cantidadinscriptos=" + p.getCantidadinscriptos()
                        + " where id=" + p.getId() 
                        + "";
		return template.update(sql);
	}

	public int delete(int id) {
		String sql = "delete from curso where id=" + id + "";
		return template.update(sql);
	}

	public Curso getCursoById(int id) {
		String sql = "select * from curso where id=?";
		return template.queryForObject(sql, new Object[] { id }, new BeanPropertyRowMapper<Curso>(Curso.class));
	}

	public List<Curso> getCursos() {
		return template.query("select * from curso", new RowMapper<Curso>() {
			public Curso mapRow(ResultSet rs, int row) throws SQLException {
				Curso e = new Curso();
				e.setId(rs.getInt(1));
				e.setDescripcion(rs.getString(2));
                                e.setSemestreanho(rs.getString(3));
                                e.setCantidadinscriptos(rs.getInt(4));
                                
                                return e;
			}
		});
	}
}
