/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaanotas.springmvc;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.uaanotas.beans.Profesor;
import com.uaanotas.dao.ProfesorDao;

/**
 *
 * @author Arnaldo Riquelme
 */

@Controller
public class ProfesorController {
  
        @Autowired
	ProfesorDao daoProfesor;

	@RequestMapping("/profesorform")
	public ModelAndView showform() {
		return new ModelAndView("profesorform", "command", new Profesor());
	}

	@RequestMapping(value = "/saveprofesor", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("profesor") Profesor profesor) {
		daoProfesor.save(profesor);
		return new ModelAndView("redirect:/viewprofesor");
	}

	@RequestMapping("/viewprofesor")
	public ModelAndView viewalumno() {
		List<Profesor> list = daoProfesor.getProfesores();
		return new ModelAndView("viewprofesor", "list", list);
	}

	@RequestMapping(value = "/editprofesor/{id}")
	public ModelAndView edit(@PathVariable int id) {
		Profesor profesor = daoProfesor.getProfesorById(id);
		return new ModelAndView("profesoreditform", "command", profesor);
	}

	@RequestMapping(value = "/editsaveprofesor", method = RequestMethod.POST)
	public ModelAndView editsave(@ModelAttribute("alumno") Profesor profesor) {
		daoProfesor.update(profesor);
		return new ModelAndView("redirect:/viewprofesor");
	}

	@RequestMapping(value = "/deleteprofesor/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable int id) {
		daoProfesor.delete(id);
		return new ModelAndView("redirect:/viewprofesor");
	}

    
    
    
}
