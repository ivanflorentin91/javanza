package com.uaanotas.springmvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.uaanotas.beans.Semestre;
import com.uaanotas.dao.SemestreDao;

@Controller
public class SemestreController {
	@Autowired
	SemestreDao daoSemestre;

	@RequestMapping("/semestreform")
	public ModelAndView showform() {
		return new ModelAndView("semestreform", "command", new Semestre());
	}

	@RequestMapping(value = "/savesemestre", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("semestre") Semestre semestre) {
		daoSemestre.save(semestre);
		return new ModelAndView("redirect:/viewsemestre");
	}

	@RequestMapping("/viewsemestre")
	public ModelAndView viewsemestre() {
		List<Semestre> list = daoSemestre.getSemestres();
		return new ModelAndView("viewsemestre", "list", list);
	}

	@RequestMapping(value = "/editsemestre/{id}")
	public ModelAndView edit(@PathVariable int id) {
		Semestre semestre = daoSemestre.getSemestreById(id);
		return new ModelAndView("semestreeditform", "command", semestre);
	}

	@RequestMapping(value = "/editsavesemestre", method = RequestMethod.POST)
	public ModelAndView editsave(@ModelAttribute("semestre") Semestre semestre) {
		daoSemestre.update(semestre);
		return new ModelAndView("redirect:/viewsemestre");
	}

	@RequestMapping(value = "/deletesemestre/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable int id) {
		daoSemestre.delete(id);
		return new ModelAndView("redirect:/viewsemestre");
	}

}