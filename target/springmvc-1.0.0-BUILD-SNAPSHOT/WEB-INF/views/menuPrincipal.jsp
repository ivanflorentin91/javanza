<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>CRUD SPRING MVC CON MYSQL</title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="col-md-6">
			<h2 class="text-center">Menu Principal</h2>
                        <h4 class="text-center">Sistema de Gesti�n del Alumno</h4>
			<hr>
			<table class="table table-bordered table-striped">
				<thead>
				<tr>
					<th>Curso</th>
					<th>Semestre</th>
					<th>Materia</th>
					<th>Alumno</th>
                                        <th>Profesor</th>
				</tr>
				</thead>
				<tbody>
				
					<tr>
					<td><a href="viewcurso" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-check"></i> Lista Cursos</a></td>
					
					<td><a href="viewsemestre" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-check"></i> Lista Semestre</a></td>
					
					<td><a href="viewmateria" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-check"></i> Lista Materia</a></td>
					
                                        <td><a href="viewalumno" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-check"></i> Lista Alumno</a></td>
                                        
                                        <td><a href="viewprofesor" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-check"></i> Lista Profesor</a></td>
                                        
                                                                               
                                        </tr>
								
				</tbody>
			</table>
			<br />
		</div>
	</div>
</div>
</body>
</html>